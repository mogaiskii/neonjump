﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct PlatformConfig
{
    public GameObject platformPrefab;
    public int probability;
}


public class LevelGenerator : MonoBehaviour
{

    public GameObject platformPrefab;
    public List<PlatformConfig> availablePlatforms = new List<PlatformConfig>();

    public Camera camera_;

    public int numberOfPlatforms;

    public float levelWidth = 3f;
    public float minY = .2f;
    public float maxY = 1.5f;

    List<GameObject> existingObjects = new List<GameObject>();

    Vector3 spawnPosition = new Vector3();

    // Start is called before the first frame update
    void Start()
    {
        CreateObjects();
    }

    void OnEnable()
    {
        EventManager.StartListening("restart", Restart);
    }

    void Restart() {
        foreach (var item in existingObjects) {
            Destroy(item);
        }
        existingObjects.Clear();
        spawnPosition = new Vector3();
        CreateObjects();
    }

    // Update is called once per frame
    void Update()
    {
        if (PauseMenu.gamePaused) return;
        int max_circles = existingObjects.Count;
        int i = 0;
        while (max_circles -- > 0)
        {
            if (i >= existingObjects.Count) break;

            Vector3 objectCameraPosition = camera_.WorldToScreenPoint(existingObjects[i].transform.position);

            if (objectCameraPosition.y < -100)  // if object was deleted - the next one would have same index
            {
                Destroy(existingObjects[i]);
                existingObjects.RemoveAt(i);
            } else
            {
                i++;
            }
        }
        if (existingObjects.Count < numberOfPlatforms)
        {
            CreateObjects();
        }
    }

    GameObject RandomPlatform()
    {
        int sum = availablePlatforms.Sum(item => item.probability);
        int target = Random.Range(0, sum);
        foreach (var item in availablePlatforms)
        {
            sum -= item.probability;
            if (sum < target)
            {
                return item.platformPrefab;
            }
        }
        return platformPrefab;
    }

    void CreateObjects()
    {
        levelWidth = 0.9f * camera_.orthographicSize * Screen.width / Screen.height;
        for (int i = 0; i < numberOfPlatforms; i++)
        {
            spawnPosition.y += Random.Range(minY, maxY);
            spawnPosition.x = Random.Range(-levelWidth, levelWidth);
            existingObjects.Add(Instantiate(RandomPlatform(), spawnPosition, Quaternion.identity));
        }
    }
}
