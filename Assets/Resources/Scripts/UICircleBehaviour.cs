﻿using UnityEngine;

public class UICircleBehaviour : MonoBehaviour
{
    bool isTouched = false;
    bool isMobile = false;
    float nonMovingTime = 0f;
    SpriteRenderer sr;

    GameObject child;

    Vector2 tempPosition; // on screen

    public GameObject sword;
    private void Awake()
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            isMobile = true;
        }
        sr = GetComponent<SpriteRenderer>();
        sr.enabled = isTouched;
        child = transform.GetChild(0).gameObject;
    }

    void OnEnable()
    {
        EventManager.StartListening("restart", Restart);
    }
    void Restart()
    {
        sr.enabled = false;
        child.GetComponent<SpriteRenderer>().enabled = false;
        nonMovingTime = 1.5f;
    }

    // Update is called once per frame
    void Update()
    {
        if (PauseMenu.gamePaused) {
            nonMovingTime = 0.2f;
            sr.enabled = false;
            child.GetComponent<SpriteRenderer>().enabled = false;
            return;
        }

        if (nonMovingTime > 0)
        {
            nonMovingTime -= Time.deltaTime;
            return;
        }

        Vector3 newPosition = tempPosition;
        if (isMobile)
        {
            if (Input.touchCount > 0)
            {
                if (isTouched is false)
                {
                    Vector3 tmp = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
                    transform.position = new Vector3(tmp.x, tmp.y, transform.position.z);
                    tempPosition = Input.GetTouch(0).position;
                    isTouched = true;
                }
                newPosition = Input.GetTouch(0).position;
            } else
            {
                isTouched = false;
            }
        } else
        {
            if (Input.GetMouseButton(0))
            {
                if (isTouched is false)
                {
                    Vector3 tmp = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    transform.position = new Vector3(tmp.x, tmp.y, transform.position.z);
                    tempPosition = Input.mousePosition;
                    isTouched = true;
                }
                newPosition = Input.mousePosition;
            }
            else
            {
                isTouched = false;
            }
        }

        if (PauseMenu.flagFromSword)
        {
            tempPosition = Camera.main.WorldToScreenPoint(
                 new Vector3(sword.transform.position.x, sword.transform.position.y, transform.position.z)
            );
        }

        sr.enabled = isTouched;
        child.GetComponent<SpriteRenderer>().enabled = isTouched;
        if (isTouched)
        {
            Vector3 tmp = Camera.main.ScreenToWorldPoint(tempPosition);
            transform.position = new Vector3(tmp.x, tmp.y, transform.position.z);
            Vector3 tmp2 = Camera.main.ScreenToWorldPoint(newPosition);
            child.transform.position = new Vector3(tmp2.x, tmp2.y, child.transform.position.z);

            SpriteRenderer childSr = child.GetComponent<SpriteRenderer>();
            if (!PauseMenu.flagActiveTop && tmp.y < tmp2.y)
            {
                childSr.color = new Color(0.8f, 0.1f, 0.1f, 0.5f);
                sr.color = new Color(0.8f, 0.1f, 0.1f, 0.5f);
            } else if (!PauseMenu.flagActiveBot && tmp2.y < tmp.y)
            {
                childSr.color = new Color(0.8f, 0.1f, 0.1f, 0.5f);
                sr.color = new Color(0.8f, 0.1f, 0.1f, 0.5f);
            } else
            {
                childSr.color = new Color(1f, 1f, 1f, 0.9f);
                sr.color = new Color(1f, 1f, 1f, 0.9f);
            }
        }
    }
}
