﻿using UnityEngine;

public class SwordBehawiour : MonoBehaviour
{
    public static int PLATFORM_LAYER = 8;

    Vector2 speedVector;
    bool isMoving = false;
    public float Speed = 3f;
    float ActualSpeed = 3f;

    Vector2 startDelta;
    Vector3 startPosition;
    float playerHeight;
    Quaternion startRotation;

    public GameObject player;
    public SpriteRenderer playerSpriteRenderer;
    // Start is called before the first frame update

    void Start()
    {
        startPosition = new Vector3(transform.position.x, transform.position.y, transform.position.z);

        UIArrowBehaviour uiArrow = GetComponentInChildren<UIArrowBehaviour>();
        uiArrow.Subscribe(HandleNeedMove);

        playerHeight = playerSpriteRenderer.bounds.size.y;
        Vector3 tempDelta = player.transform.position - transform.position;
        startDelta = new Vector2(tempDelta.x, tempDelta.y);
        startRotation = new Quaternion(transform.rotation.x, transform.rotation.y, transform.rotation.z, transform.rotation.w);
    }
    void OnEnable()
    {
        EventManager.StartListening("restart", Restart);
    }

    void Restart() {
        isMoving = false;
        transform.position = new Vector3(startPosition.x, startPosition.y, startPosition.z);
        transform.rotation = new Quaternion(startRotation.x, startRotation.y, startRotation.z, startRotation.w);
    }

    void HandleNeedMove(float rotation)
    {
        if (isMoving) return;
        
        float sin = Mathf.Sin(Mathf.Deg2Rad * rotation);
        float cos = Mathf.Cos(Mathf.Deg2Rad * rotation);
        if (rotation > 89f && rotation < 91f)
            cos = 0;
        if (rotation < 1 || rotation > 179)
        {
            sin = 0;
            if (rotation < 1)
            {
                cos = 1;
            } else
            {
                cos = -1;
            }
        }

        ActualSpeed = Speed + SmoothCamera.y_speed * 0.8f;

        speedVector = new Vector2(cos * 0.1f, sin * 0.1f);
        transform.Rotate(0, 0, rotation - transform.eulerAngles.z - 90);
        isMoving = true;
    }

    void OnCollisionEnter2D(Collision2D other) {
        if (other.gameObject.layer == PLATFORM_LAYER) {
            PlatformBehaviour platform = other.gameObject.GetComponent<PlatformBehaviour>();
            if (platform.CanJump())
            {
                PlayerJump(other);
            }
            ReturnToPlayer();
            platform.OnHit();
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (PauseMenu.gamePaused) return;

        if (isMoving)
        {
            transform.position = new Vector3(
                transform.position.x + speedVector.x * Time.deltaTime * ActualSpeed,
                transform.position.y + speedVector.y * Time.deltaTime * ActualSpeed,
                transform.position.z
            );
            
        }

        Vector3 screenPoint = Camera.main.WorldToViewportPoint(transform.position);
        if (screenPoint.x < -0.1 || screenPoint.x > 1.1 || screenPoint.y < 0 || screenPoint.y > 1)
        {
            if (PauseMenu.swordDeath || (screenPoint.y < 0 && PauseMenu.cameraDeath))
            {
                EventManager.TriggerEvent("restart");
            } else
            {
                ReturnToPlayer();
            }
        }
    }

    void ReturnToPlayer()
    {
        isMoving = false;
        transform.position = new Vector3(
            player.transform.position.x - startDelta.x,
            player.transform.position.y - startDelta.y,
            transform.position.z
        );
        transform.rotation = startRotation;
    }

    void PlayerJump(Collision2D collision)
    {
        player.transform.position = new Vector3(
            collision.transform.position.x,
            collision.transform.position.y + playerHeight,
            player.transform.position.z
        );
    }
}
