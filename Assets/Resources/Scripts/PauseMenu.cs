﻿using UnityEngine;

public class PauseMenu : MonoBehaviour
{
    public static bool gamePaused = false;
    public static bool flagFromSword = true;
    public static bool flagActiveTop = true;
    public static bool flagActiveBot = true;
    public static bool swordDeath = false;
    public static bool cameraDeath = true;
    public GameObject pauseMenu;

    private void Awake()
    {
        pauseMenu.SetActive(false);
    }

    // Start is called before the first frame update
    void Start()
    {
        EventManager.StartListening("menu", ToggleMenu);
    }

    public void SetFromSword(bool value)
    {
        flagFromSword = value;
    }

    public void SetActiveBot(bool value)
    {
        flagActiveBot = value;
    }

    public void SetActiveTop(bool value)
    {
        flagActiveTop = value;
    }
    public void SetSwordDeath(bool value)
    {
        swordDeath = value;
    }
    public void SetCameraDeath(bool value)
    {
        cameraDeath = value;
    }

    public void ToggleMenu()
    {
        gamePaused = !gamePaused;

        pauseMenu.SetActive(gamePaused);
        if (gamePaused)
        {
            Time.timeScale = 0f;
        } else
        {
            Time.timeScale = 1f;
        }
    }
}
