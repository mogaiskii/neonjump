﻿using UnityEngine;

public class Player : MonoBehaviour
{
    Vector3 startPosition;
    public bool deathOnScreen = false;
    // Start is called before the first frame update
    void Start()
    {
        startPosition = new Vector3(transform.position.x, transform.position.y, transform.position.z);
    }
    void OnEnable()
    {
        EventManager.StartListening("restart", Restart);
    }
    void Restart()
    {
        Rigidbody2D rb = GetComponent<Rigidbody2D>();
        rb.velocity = new Vector2(0, 0);
        transform.position = new Vector3(startPosition.x, startPosition.y, startPosition.z);
    }
    private void Update()
    {
        if (PauseMenu.gamePaused) return;
        Vector3 screenPoint = Camera.main.WorldToViewportPoint(transform.position);
        if (deathOnScreen)
        {
            if (screenPoint.y > 0.9)
            {
                EventManager.TriggerEvent("restart");
            }
        }
    }
}
