﻿using UnityEngine;

public class UIArrowBehaviour : MonoBehaviour
{
    public delegate void NeedMovingTo(float rotation);
    event NeedMovingTo Notify;


    bool isMobile = false;
    Vector3? startPosition = null;
    float rotate = 0;
    SpriteRenderer spriteRenderer;
    bool isVisible = false;
    Quaternion startRotation;
    float nonMovingTime;
    bool isUp = false;

    public GameObject sword;

    public void Subscribe(NeedMovingTo func)
    {
        Notify += func;
    }
    void OnEnable()
    {
        EventManager.StartListening("restart", Restart);
    }
    void Restart()
    {
        Cleanup();
        nonMovingTime = 1.5f;
    }

    private void Awake()
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            isMobile = true;
        }
        spriteRenderer = GetComponent<SpriteRenderer>();
        spriteRenderer.enabled = isVisible;
        rotate = 90;
        transform.Rotate(0, 0, rotate - transform.eulerAngles.z);
        startRotation = new Quaternion(transform.rotation.x, transform.rotation.y, transform.rotation.z, transform.rotation.w);
    }

    void Cleanup()
    {
        startPosition = null;
        isVisible = false;
        transform.rotation = new Quaternion(startRotation.x, startRotation.y, startRotation.z, startRotation.w);
    }

    // Update is called once per frame
    void Update()
    {
        if (PauseMenu.gamePaused) {
            Cleanup();
            nonMovingTime = 0.2f;
            spriteRenderer.enabled = false;
            return;
        }

        if (nonMovingTime > 0)
        {
            nonMovingTime -= Time.deltaTime;
            return;
        }

        Vector3? position = null;
        if (isMobile)
        {
            // TODO:
            if (Input.touchCount > 0)
                position = Input.touches[0].position;
        }
        else
        {
            if (Input.GetMouseButton(0))
                position = Input.mousePosition;
        }

        if (position.HasValue)
        {
            isVisible = true;
            if (startPosition.HasValue)
            {
                
                if (Vector3.Distance(position.Value, startPosition.Value) > 0.8f)
                {
                    rotate = Vector3.Angle(
                        new Vector3(position.Value.x - startPosition.Value.x, position.Value.y - startPosition.Value.y, 0),
                        new Vector3(1, 0, 0)
                    );
                    isUp = false;
                    if (position.Value.y <= startPosition.Value.y) {
                        rotate = 180f - rotate;
                        spriteRenderer.color = new Color(1, 1, 1, 0.5f);
                        isUp = true;
                    }
                    transform.Rotate(0, 0, rotate - transform.eulerAngles.z);
                }

            } else
            {
                startPosition = position;
            }

            if (PauseMenu.flagFromSword)
            {
                startPosition = Camera.main.WorldToScreenPoint(
                   new Vector3(sword.transform.position.x, sword.transform.position.y, transform.position.z)
                );
            }
        } else if (startPosition.HasValue)
        {
            Cleanup();
            if (PauseMenu.flagActiveBot && isUp)
            {
                Notify?.Invoke(rotate);
            } else if (PauseMenu.flagActiveTop && !isUp)
            {
                Notify?.Invoke(rotate);
            }
        }
        spriteRenderer.enabled = isVisible;
    }
}
