﻿using UnityEngine;

class TrapStrategy : PlatformStrategy
{

    public override bool CanJump()
    {
        return false;
    }

    public override void OnHit(GameObject owner)
    {
        owner.SetActive(false);
    }
}
