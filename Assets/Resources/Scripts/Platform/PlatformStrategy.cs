﻿using UnityEngine;

public class PlatformStrategy
{
    public virtual Vector3 GetPosition(Vector3 oldPos)
    {
        return oldPos;
    }

    public virtual bool CanJump()  // one day this behaviour would depends on args..
    {
        return true;
    }

    public virtual void OnHit(GameObject owner)
    {
    }
}

