﻿using UnityEngine;

public class MovingStrategy : PlatformStrategy
{
    int movingV = 0;
    int movingH = 0;
    int speed = 5;

    int maxRadius = 7;
    float passedRadius = 0;

    public MovingStrategy()
    {
        if (Random.Range(0, 10) > 4) movingH = 1;
        else movingV = 1;

        if (Random.Range(0, 999) == 998) movingV = movingH = 1;
    }

    public override Vector3 GetPosition(Vector3 oldPos)
    {
        if (movingH != 0)
        {
            Vector3 pos = Camera.main.WorldToViewportPoint(oldPos);
            if (pos.x < 0.1)
            {
                movingH = 1;
            }
            if (pos.x > 0.9)
            {
                movingH = -1;
            }
        }

        float passedX = movingH * Time.deltaTime * speed;
        float passedY = movingV * Time.deltaTime * speed;

        if (movingV != 0)
        {
            passedRadius += Mathf.Abs(passedY);
            if (passedRadius > maxRadius)
            {
                movingV = -movingV;
                passedRadius = 0;
            }
        }


        return new Vector3(
            oldPos.x + passedX,
            oldPos.y + passedY,
            oldPos.z
            );
    }

    public override void OnHit(GameObject owner)
    {
        speed = 0;
        movingV = 0;
        movingH = 0;
    }
}
