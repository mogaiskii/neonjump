﻿using UnityEngine;



public enum StrategyTrick
{
    normal, moving, trap
}


public class PlatformBehaviour : MonoBehaviour
{
    public StrategyTrick strategyTrick = StrategyTrick.normal;
    PlatformStrategy strategy;
    private void Awake()
    {
        if (strategyTrick == StrategyTrick.normal) strategy = new PlatformStrategy();
        else if (strategyTrick == StrategyTrick.moving) strategy = new MovingStrategy();
        else if (strategyTrick == StrategyTrick.trap) strategy = new TrapStrategy();
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = strategy.GetPosition(transform.position);
    }

    public void OnHit()
    {
        strategy.OnHit(gameObject);
    }

    public bool CanJump()
    {
        return strategy.CanJump();
    }
}
