﻿using UnityEngine;

public class SmoothCamera : MonoBehaviour
{
    public Transform player;

    public float smoothSpeed = 0.1f;
    public Vector3 offset { get; private set; }
    Vector3 startPosition;
    public float simpleSpeed = 0.1f;
    float nonMovingTreshold = 0f;

    public static float y_position = 0;
    public static float y_speed = 0;

    public float currentSpeed { get { return CalculateSpeed(); } private set { } }

    // Start is called before the first frame update
    void Start()
    {
        offset = (transform.position - player.position) / 3;
        nonMovingTreshold = 0f;
        startPosition = new Vector3(transform.position.x, transform.position.y, transform.position.z);
    }
    void OnEnable()
    {
        EventManager.StartListening("restart", Restart);
    }
    void Restart() {
        transform.position = new Vector3(startPosition.x, startPosition.y, startPosition.z);
        nonMovingTreshold = 3f;
    }

    float CalculateSpeed()
    {
        float x = transform.position.y + 10;
        y_position = x;

        float speed = (((10/x + Mathf.Sqrt(x / 4) * 4) * 2 + x/10 + Mathf.Sqrt(x / 2) * 2 / x) * 0.5f + x/50) * 2;
        y_speed = speed;
        return speed;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (PauseMenu.gamePaused)
        {
            nonMovingTreshold = 0.2f;
            return;
        }

        if (nonMovingTreshold > 0)
        {
            nonMovingTreshold -= Time.deltaTime;
        }else
        {
            float yPosition = player.position.y + offset.y;

            if (PauseMenu.cameraDeath)
            {
                yPosition = transform.position.y;
                if (player.position.y + offset.y > transform.position.y)
                {
                    yPosition = player.position.y + offset.y;
                }
                yPosition += CalculateSpeed() * Time.deltaTime;
            }

            Vector3 desiredPosition = new Vector3(transform.position.x, yPosition, offset.z);
            Vector3 smoothPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed);
            transform.position = new Vector3(smoothPosition.x, smoothPosition.y, transform.position.z);
        }
    }
}
